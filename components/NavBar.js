import { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';

import UserContext from '../UserContext';

export default function NavBar(){
	const { user } = useContext(UserContext);

	return(
		<Navbar bg="warning" expand="lg" className="navbar navbar-expand-lg fixed-top">
            {(user.email)
            ?
                <img src="/tpc.png" alt="Nyaw!" className="img-fluid rounded mx-auto d-block" id="catNav"/>
            :
            <Link href="/">
                <img src="/tpc.png" alt="Nyaw!" className="img-fluid rounded mx-auto d-block" id="catNav"/>
            </Link>
            }
			
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
				
				    {(user.email)
                    ?
                        <React.Fragment>
                        <Link href="/home">
                            <a className="nav-link" role="button">Home</a>
                        </Link>
                            <Link href="/categories">
                                <a className="nav-link" role="button">Categories</a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                        </React.Fragment>
                    :
                        <React.Fragment>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </React.Fragment>
                    }  
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}