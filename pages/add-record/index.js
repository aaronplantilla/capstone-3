import {useState, useEffect} from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import View from '../../components/View'
import Router from 'next/router'
import Head from 'next/head'
import Swal from 'sweetalert2'
import PropTypes from 'prop-types';
import AppHelper from '../../app-helper'

export default function index() {
	const [categoryName, setCategoryName] = useState('')
	const [categoryType, setCategoryType] = useState('')
	const [amount, setAmount] = useState(0)
	const [description, setDescription] = useState('')
	const [balanceAfterTransaction, setBalanceAfterTransaction] = useState(0)

	console.log(categoryName)
	console.log(categoryType)
	console.log(amount)
	console.log(description)
	console.log(balanceAfterTransaction)
 
//state to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if(categoryName !== '' && categoryType !== '' && amount !== 0 && description !== ''){
            setIsActive(true);
        }else{
            setIsActive(false)
        }
    }, [categoryName, categoryType, amount, description])

//function to add category
	function newRecord(e) {
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URL}users/add-record`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				categoryName: categoryName,
				typeName: categoryType,
				amount: amount,
				description: description,
				balanceAfterTransaction: balanceAfterTransaction

			})
		})
		.then(res => res.json())
		.then(data => {
			//add successful
			if(data === true){
				Swal.fire({
				title: 'NYAN!! Record has been added.',
				width: 600,
				padding: '3em',
				background: '#fff url(https://sweetalert2.github.io/images/trees.png)',
				backdrop: `
				rgba(0,0,123,0.4)
				url("https://64.media.tumblr.com/8210fd413c5ce209678ef82d65731443/tumblr_mjphnqLpNy1s5jjtzo1_400.gifv")
				left top
				no-repeat
				`
				})
				Router.push('/home')
			}else{
				//error in creating registration, redirect to error page
				Swal.fire({
				  icon: 'error',
				  title: 'Woops!',
				  text: 'Adding a new record failed!',
				})
			}
		})
	}

	// fetch categories data
    const [categories, setCategories] = useState([])

	useEffect(() => {
		fetch(`${ AppHelper.API_URL }users/get-categories`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
		}).then(AppHelper.toJSON)
		.then(data => {
			setCategories(data)
	    })

	}, [])

// Income Filter
	const incomeFetch = categories.filter(category => category.typeName === 'Income')
	const incomePush = incomeFetch.map(income => {
		return(
			<option key={income._id}>{income.name}</option>
		)
	})

// Expense Filter
	const expenseFetch = categories.filter(category => category.typeName === 'Expense')
	const expensePush = expenseFetch.map(expense => {
		return(
			<option key={expense._id}>{expense.name}</option>
		)
	})

    return (
    	<React.Fragment>
	    	<View title={ 'Add New Record | The Thrifty Cat' } />
	    	<Container>
		    	<Form onSubmit={(e) => newRecord(e)}>
		    		<Form.Group controlId="type">
		            	<Form.Label className="text-light"><strong>Type</strong></Form.Label>
						<Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
							<option>-</option>
							<option>Income</option>
							<option>Expense</option>
						</Form.Control>
		            </Form.Group>

					<Form.Group controlId="categoryName">
		            	<Form.Label className="text-light"><strong>Category</strong></Form.Label>
		            	{categoryType == 'Expense' ?
			            	<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)}>
								{expensePush}
							</Form.Control>
						:
						categoryType == 'Income' ?
							<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)}>
								{incomePush}
							</Form.Control>
						:
							<Form.Control as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)}>
								<option>-</option>
							</Form.Control>
		            	}
		            </Form.Group>
		           
		    	 	<Form.Group controlId="amount">
		                <Form.Label className="text-light"><strong>Amount</strong></Form.Label>
		                <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required/>
		            </Form.Group>
		            <Form.Group controlId="description">
		                <Form.Label className="text-light">Description</Form.Label>
		                <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
		                <Form.Text className="text-muted">
	                    	Give at least a short description.
	                    </Form.Text>
		            </Form.Group>
		            
		            {isActive ?
		                <Button variant="success" type="submit" id="submitBtn">Add</Button>
		                :
		                <Button variant="primary" type="submit" id="submitBtn" disabled>Add</Button>
		            }
		        </Form>
	    	</Container>
    	</React.Fragment>
    )
}