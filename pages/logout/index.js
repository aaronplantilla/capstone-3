import { useContext, useEffect } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import Swal from 'sweetalert2'

export default function index(){
	const {unsetUser, setUser} = useContext(UserContext)

	useEffect(() => {
		unsetUser();
		Swal.fire({
		  title: 'Session Ended!!',
		  text: 'Please come back again. :(',
		  imageUrl: 'https://i.pinimg.com/originals/58/ab/47/58ab47860efae32bfaaa887b6b00b53a.gif',
		  imageAlt: 'Custom image',
		})
		Router.push('/')
	}, [])

	return null
}