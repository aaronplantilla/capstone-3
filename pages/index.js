import Head from 'next/head';
import Router from 'next/router';
import Link from 'next/link';
import { Container, Button } from 'react-bootstrap';


export default function index() {
  return (
    <React.Fragment>
    <Head>
    	<title>The Thrifty Cat | Manage Your Needs And Wants!</title>
    </Head>
    <div id="bg">
		<h1 className="text-center" id="landingTitle">The Thrifty</h1>
		<h1 className="text-center" id="catLandingTitle">Cat</h1>
		<Container fluid id="landingBackground" className="m">
			<div id="items">
				<img src="/tpc.png" alt="Nyaw!" className="img-fluid rounded mx-auto d-block" id="catLanding"/>
			</div>
		</Container>
		<div id="getStarted">
			<Link href="/login">
				<Button variant="warning" size="lg" block>Manage Your Needs And Wants Now!</Button>
			</Link>
		</div>
	</div>
    </React.Fragment>
  )
}
