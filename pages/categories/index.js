import {useState, useEffect} from 'react';
import { Button, Container, Table } from 'react-bootstrap';
import Head from 'next/head'
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper'
import Link from 'next/link'

export default function index(){

	const [categories, setCategories] = useState([])

	useEffect(() => {
		fetch(`${ AppHelper.API_URL }users/get-categories`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
		}).then(AppHelper.toJSON)
		.then(data => {
			setCategories(data)
	    })

	}, [])

	const dataRows = categories.map(category => {
		return(
			<tr key={category._id}>
				<td>{category.name}</td>
				<td>{category.typeName}</td>
			</tr>
		)

	})

	return (
		<Container>
			<Head>
				<title>Categories | The Thrifty Cat</title>
			</Head>
			<h1 className="text-center text-light">Categories List</h1>
			<Link href="/add-category">
				<Button variant="success">Add New Category</Button>
			</Link>
			<Table striped bordered hover>
				<thead>
					<tr className="table-warning">
						<th className="text-primary">NAME</th>
						<th className="text-primary">TYPE</th>
					</tr>
				</thead>
				{dataRows == '' ?
				<tbody>
					<tr>
						<td>No records to display.</td>
						<td>No records to display.</td>
					</tr>
				</tbody>
				:
				<tbody>
					{dataRows}
				</tbody>
				}
				
			</Table>
		</Container>
	)
}