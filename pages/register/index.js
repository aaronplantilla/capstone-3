import {useState, useEffect} from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import View from '../../components/View'
import Router from 'next/router'
import Head from 'next/head'
import Swal from 'sweetalert2'

export default function index() {

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    //state to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false)
        }
    }, [firstName, lastName, email, password1, password2])

	//function to register user
	function registerUser(e) {
		e.preventDefault();

		//check for duplicate email in database first
		fetch(`${process.env.NEXT_PUBLIC_API_URL}users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
			})
		})
		.then(res => res.json())
		.then(data => {
		//if no duplicates found
		if (data === false){
		fetch(`${process.env.NEXT_PUBLIC_API_URL}users`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1
				})
		})
		.then(res => res.json())
		.then(data => {
			//registration successful
			if(data === true){
				//redirect to login
				Swal.fire(
		  		'Nyan!!',
		  		'You are now registered.',
		  		'success'
				)
				Router.push('/login')
			}else{
				//error in creating registration, redirect to error page
				Router.push('/error')
			}
		})
			}else{ //duplicate email found
				Router.push('/error')
			}
		})
	} 

    return (
    	<React.Fragment>
	    	<View title={ 'Register | The Thrifty Cat' } />
	    	<Container>
		    	<Form onSubmit={(e) => registerUser(e)}>
		    	 	<Form.Group controlId="firstName">
		                <Form.Label>First Name</Form.Label>
		                <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
		            </Form.Group>

		            <Form.Group controlId="lastName">
		                <Form.Label>Last Name</Form.Label>
		                <Form.Control type="text" placeholder="Enter first name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
		            </Form.Group>

		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
		                <Form.Text className="text-muted">
		                We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
		            </Form.Group>

		            <Form.Group controlId="password2">
		                <Form.Label>Verify Password</Form.Label>
		                <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
		            </Form.Group>

		            {isActive ?
		                <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
		                :
		                <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
		            }
		        </Form>
	    	</Container>
    	</React.Fragment>
    )
}