import {useState, useEffect} from 'react';
import { Button, Container, Table, Card } from 'react-bootstrap';
import Head from 'next/head'
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper'
import Link from 'next/link'
import moment from 'moment'
import BarChart from '../../components/BarChart'
import BarChartExpense from '../../components/BarChartExpense'

export default function index(){

	const [records, setRecords] = useState([])

	useEffect(() => {
		fetch(`${ AppHelper.API_URL }users/get-most-recent-records`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
		}).then(AppHelper.toJSON)
		.then(data => {
			setRecords(data)
	    })

	}, [])

	const dataRows = records.map(record => {
		return(
			<tr key={record._id}>
				<td>{record.categoryName}</td>
				<td>{record.typeName}</td>
				<td>{record.description}</td>
				<td>{moment(record.dateAdded).format('LLL')}</td>
				{record.typeName == 'Income' ?
				<td className="text-success">+&#8369;{record.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
				:
				<td className="text-danger">-&#8369;{record.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
				}
			</tr>
		)

	})

	console.log(dataRows)

	const balance = records.map(record => {
		return(
			record.balanceAfterTransaction.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
		)
	})

	console.log(balance)


	return(
		<React.Fragment>
		<Container>
			<Head>
				<title>Home | The Thrifty Cat</title>
			</Head>
			<h1 className="text-center text-light">Current Balance:</h1>
			{ balance == 0 ?
				<h1 className="text-center text-warning">&#8369; 0</h1>
			:
				<h1 className="text-center text-warning">&#8369; {balance[0]}</h1>
			}
		<Link href="/search-record">
			<Button variant="warning" className="btn-secondary btn-lg text-primary" type="submit">Search</Button>
		</Link>
		<Link href="/add-record">
				<Button variant="success" className="btn-lg" inline>Add New Record</Button>
			</Link>
			<Table striped bordered hover>
				<thead>
					<tr className="table-warning">
						<th className="text-primary">CATEGORY NAME</th>
						<th className="text-primary">CATEGORY TYPE</th>
						<th className="text-primary">DESCRIPTION</th>
						<th className="text-primary">TRANSACTION DATE</th>
						<th className="text-primary">AMOUNT</th>
					</tr>
				</thead>
				{dataRows == '' ?
				<tbody>
					<tr>
						<td>No records to display.</td>
						<td>No records to display.</td>
						<td>No records to display.</td>
						<td>No records to display.</td>
						<td>No records to display.</td>
					</tr>
				</tbody>
				:
				<tbody>
					{dataRows.slice(0, 5)}
				</tbody>
				}
				
			</Table>
			<Card>
			<Card.Body>
			<BarChart />
			</Card.Body>
			<Card.Body>
			<BarChartExpense />
			</Card.Body>
			</Card>
			
		</Container>
		</React.Fragment>
	)
}